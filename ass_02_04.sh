#!/bin/bash

echo "Enter a name: "
read name

if [ ! -e $name ]
then
	echo "please enter valid file name"
fi

if [ -f $name ]
then
	res=`stat -c %G $name`
	echo "file : $name (Owner : $res)"
else
	cd $name
	ls
fi
    


#!/bin/bash

 #   shell script to search element from array and also find duplicate elements.
 
 	echo -n -e "Enter array size : \n"
  	read size
 	i=0
  	while [ $i -lt $size ]
 	do
 	    echo -e "Enter element $i : "
 	    read arr[$i]
 	    i=`expr $i + 1`
 	done
 	
 	echo -n "Enter array : "
 	for num in ${arr[*]}
 	do
 	    echo -n "$num, "
 	done
 	echo -e "\n"
 	
 	echo -n "Duplicate elements present in array : "
 	f=0
 	i=0
 	while [ $i -lt `expr $size - 1` ]
 	do
 	    j=`expr $i + 1`
 	    k=0
 	    while [ $k -lt $i ]
 	    do
 	        if [ ${arr[$k]} -eq ${arr[$i]} ]
 	        then
	            i=`expr $i + 1`
 	            continue 2
	        fi
 	        k=`expr $k + 1`
 	    done
 	    while [ $j -lt $size ]
 	    do
	        if [ ${arr[$j]} -eq ${arr[$i]} ]
 	        then
 	            echo -n "${arr[$i]}, "
 	            f=1
 	            i=`expr $i + 1`
	            continue 2
 	        fi
 	        j=`expr $j + 1`
 	    done
 	         i=`expr $i + 1`
	done

 	if [ $f -eq 0 ]
	then
	    echo "No duplicate element present"
 	fi

 	echo -e "\n"
 
 	echo -n -e "Enter element to search : \n"
	read search

 	i=0
 	j=0
 	echo -n "Search element is present at index no :  "
 	for num in ${arr[*]}
 	do
 	    if [ $num -eq $search ]
 	    then
 	        echo -n "$i, "
 	        j=`expr $j + 1`
 	    fi
 	    i=`expr $i + 1`
 	done
 	echo -e "\n"
 
 	if [ $j -eq 0 ]
	then
	    echo "Element not found in array"
	 fi

#!/bin/bash

# print table of given number.

# initialization
# while [ condition ]
# do
#	...
#	modification
# done

# In while loop, if condition is true, loop body is repeated.
# If condition is false, it comes out of loop.

# initialization
# until [ condition ]
# do
#	...
#	modification
# done

# In until loop, if condition is false, loop body is repeated.
# If condition is true, it comes out of loop.

echo -n "enter a number: "
read num

i=1
while [ $i -le 10 ]
do
	res=`expr $num \* $i`
	echo "$num * $i = $res"
	i=`expr $i + 1`
done

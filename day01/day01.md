
# Embedded Operating System
* Why? Goals:
	* Understand how OS works on Embedded processors like ARM.
	* Linux Operating System + Linux programming.
* What? Contents/Syllabus:
	* OS embedded hardware interfacing.
	* Linux commands and shell scripts.
	* Linux system call programming
		* Process & Threads related syscalls.
		* File & FileSystem related syscalls.
		* Memory management related syscalls.
		* POSIX library functions.
* Pre-requisites
	* Technical knowledge
		* Linux commands - Devendra Sir
		* Computer fundamentals - Dr. Gokhale Sir
		* ARM 7 or Cortex A achitecture - Nilesh
		* C Programming - Devendra Sir
		* Operating System - CCAT Section B (Galvin) + Dr. Gokhale Sir + Sachin Pawar Sir
	* Setup
		* Linux (Ubuntu 18.04 or CEntOS 7.3)
		* VI editor (or any other editor), GCC, make, gdb or valgrind, ...
		* Chrome browser -- Student portal -- Online sessions  -- Code
* How ?
	* Lecture
		* Concepts + Linux
		* Code (in class)
		* Guidelines
			* Join lecture in time (10 mins before) on Laptop.
				* Keep ready: Student portal (code sharing), IDE & compiler, Notebook, ...
			* Q & A
				* Resond in class (Yes or No)
				* Questions on topic: Write in notebook and ask at the end of session.
				* Lab related questions: During lab session (Lab mentors + Lecture faculty).
			* Recorded videos are only for network issues or revisions.
	* Lab
		* Assignments

## Steps to learn OS
* step 1. End user
	* Operate the OS
	* Confirmatory test:
		* Format --> Windows
			* Drive
			* Capacity
			* File System
			* Label
			* Default allocation size
			* Quick Format
	* Topic: Linux commands
* step 2. Administrator
	* Installation and configuration of OS
	* Topic: Linux shell scripts
* step 3. Programmer
	* System call programming
	* Topic: File System, Process, Threads, ...
* step 4. Designer
	* Kernel internals


























